
# README for MSFF (Metal Solution Force Field)

## Purpose : 
Parameter set for Metals in Solution.

## Files :
MSFF.parm
MSFF.top
MSFF_CHARMM.inp

## Reference :
Richard P. Matthews and Kevin J. Naidoo. “Experimentally Consistent Ion Association Predicted for Metal Solutions from Free Energy Simulations” J. Phys. Chem. B. 2010, 114(21), 7286-7293.
DOI: 10.1021/jp911823x
